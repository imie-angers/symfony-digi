<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Car; 
use App\Form\CarType;

class CarsController extends AbstractController
{
    /**
     * @Route("/", name="cars")
     * @Template("cars/index.html.twig")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $cars = $em
            ->getRepository(Car::class)
            ->findAll()
        ;

        return [
            'cars' => $cars,
        ];
    }

    /**
     * @Route("/cars/{id}/view", name="cars_view")
     * @Template("cars/view.html.twig")
     * @ParamConverter("car", class=Car::class)
     */
    public function view(Car $car)
    {
        return [
            'car' => $car,
        ];
    }

    /**
     * @Route("/cars/{id}/remove", name="cars_remove")
     * @ParamConverter("car", class=Car::class)
     */
    public function remove(Car $car)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($car);
        $em->flush();

        return $this->redirectToRoute("cars");
    }

    /**
     * @Route("/cars/create", name="cars_create")
     * @Template("cars/create.html.twig")
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $car = new Car();

        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($car);
            $em->flush();

            return $this->redirectToRoute("cars");
        }

        return [
            "form" => $form->createView(),
        ];
    }
}





